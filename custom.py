import torch
import torch.utils.data as data
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo
from torch.nn.parameter import Parameter
from torch.autograd import Variable
from IPython import embed
import os

class CLSNetwork(nn.Module):
    def __init__(self):
        super(CLSNetwork, self).__init__()
        self.features = nn.Sequential(
            nn.Linear(512, 4096),
            nn.ReLU(inplace=True),
            #nn.Dropout(p=0.2, inplace=True),
            nn.Linear(4096, 51),
            nn.LogSoftmax(dim=1)
        )

    def forward(self, x):
        y = self.features(x)

        return y

class RNNNetwork(nn.Module):
    def __init__(self, num_hidden):
        super(RNNNetwork, self).__init__()
        self.num_hidden = num_hidden
        self.lstm = nn.LSTM(512, num_hidden)
        self.dp1 = nn.Dropout(p=0.7, inplace = False)
        self.fc1 = nn.Linear(num_hidden * 10, 51)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.lstm(x)[0]
        x = x.view(-1, self.num_hidden * 10)
        self.dp1(x)
        x = self.fc1(x)
        x = self.softmax(x)

        return x
