from __future__ import print_function, division
import os
import torch
import pickle
import numpy as np
from torch.utils.data import Dataset, DataLoader
from IPython import embed

class myDataset(Dataset):

    def __init__(self, feature_file):
        data = pickle.load(open(feature_file, 'rb'), encoding='iso-8859-1')
        data = data['data']
        self.features = [t['features'] for t in data]
        self.features = np.vstack(self.features)
        try:
            self.labels = [t['class_num'] for t in data]
        except:
            self.labels = np.zeros((len(self.features)), dtype=np.long)
        else:
            self.labels = np.repeat(self.labels, 10)
        self.one_hot_label = np.zeros((len(self.features), 51), dtype=np.long)
        self.one_hot_label[np.arange(len(self.features)), self.labels] = 1

    def __len__(self):
        return self.features.shape[0]

    def __getitem__(self, idx):
        features = self.features[idx]
        labels = self.labels[idx]

        return features, labels

class RNNDataset(Dataset):

    def __init__(self, feature_file):
        data = pickle.load(open(feature_file, 'rb'), encoding='iso-8859-1')
        data = data['data']
        self.features = [t['features'] for t in data]
        try:
            self.labels = [t['class_num'] for t in data]
        except:
            self.labels = np.zeros((len(self.features)), dtype=np.long)
        self.one_hot_label = np.zeros((len(self.features), 51), dtype=np.long)
        self.one_hot_label[np.arange(len(self.features)), self.labels] = 1

    def __len__(self):
        return len(self.features)

    def __getitem__(self, idx):
        features = self.features[idx]
        labels = self.labels[idx]

        return features, labels
